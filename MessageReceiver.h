#ifndef MESSAGERECEIVER_H
#define MESSAGERECEIVER_H

#include "Dynamic Array/DynamicArray.h"

class CMessenger;
class CListener;
class CMessage;

class CMessageReceiver
{
private:
	struct SListenerMessageTypePair
	{
		static SListenerMessageTypePair	ms_oNull;

		CMessenger*	m_pMessenger;
		CListener*	m_pListener;
		u32			m_uMessageType;
	};
	typedef TDynamicArray< SListenerMessageTypePair > CListenerArray;

public:
				CMessageReceiver();
	virtual		~CMessageReceiver();
	void		StopListeningToAll();
	template< class ObjectType >
	void		Listen( ObjectType& rObject, CMessenger& rMessenger, u32 uMessageType, 
					void ( ObjectType::*FPush )( CMessage&, CMessenger& ) );
	template< class ObjectType >
	void		Listen( ObjectType& rObject, CMessenger& rMessenger, u32 uMessageType, 
					void ( ObjectType::*FPush )( CMessage& ) );
	template< class ObjectType >
	void		Listen( ObjectType& rObject, CMessenger& rMessenger, u32 uMessageType, 
					void ( ObjectType::*FPush )() );
	template< class ObjectType >
	void		Listen( ObjectType& rObject, CMessenger& rMessenger, u32 uMessageType, 
					   void ( ObjectType::*FPush )() const );
	template< class ObjectType >
	void		StopListening( ObjectType& rObject, CMessenger& rMessenger, 
					u32 uMessageType, 
					void ( ObjectType::*FPush )( CMessage&, CMessenger& ) );
	template< class ObjectType >
	void		StopListening( ObjectType& rObject, CMessenger& rMessenger, 
					u32 uMessageType, void ( ObjectType::*FPush )( CMessage& ) );
	template< class ObjectType >
	void		StopListening( ObjectType& rObject, CMessenger& rMessenger, 
					u32 uMessageType, void ( ObjectType::*FPush )() );
	template< class ObjectType >
	void		StopListening( ObjectType& rObject, CMessenger& rMessenger, 
							  u32 uMessageType, void ( ObjectType::*FPush )() const );
	template< class ObjectType >
	bool		IsListening( const ObjectType& krObject, CMessenger& rMessenger,
					u32 uMessageType, void ( ObjectType::*FPush )( CMessage&, CMessenger& ) ) const;
	template< class ObjectType >
	bool		IsListening( const ObjectType& krObject, CMessenger& rMessenger, 
					u32 uMessageType, void ( ObjectType::*FPush )( CMessage& ) ) const;
	template< class ObjectType >
	bool		IsListening( const ObjectType& krObject, CMessenger& rMessenger, 
					u32 uMessageType, void ( ObjectType::*FPush )() ) const;
	template< class ObjectType >
	bool		IsListening( const ObjectType& krObject, CMessenger& rMessenger, 
							u32 uMessageType, void ( ObjectType::*FPush )() const ) const;
	void		Pause();
	void		Resume();
	bool		IsPaused() const		{ return m_bPaused; }
	void		StopListeningToMessenger( CMessenger& rMessenger );

private:
	CListenerArray	m_oListeners;
	bool			m_bPaused;
};

#endif // MESSAGERECEIVER_H
