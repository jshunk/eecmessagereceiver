template< class ObjectType >
void CMessageReceiver::Listen( ObjectType& rObject, CMessenger& rMessenger, 
	u32 uMessageType, void ( ObjectType::*FPush )( CMessage&, CMessenger& ) )
{
	SListenerMessageTypePair oListener;
	oListener.m_pListener = 
		new TListenerDelegator3< ObjectType >( rObject, FPush );
	assert( oListener.m_pListener );
	oListener.m_uMessageType = uMessageType;
	oListener.m_pMessenger = &rMessenger;
	m_oListeners.Add( oListener );
	if( !m_bPaused )
	{
		rMessenger.Listen( *oListener.m_pListener, uMessageType );
	}
}

template< class ObjectType >
void CMessageReceiver::Listen( ObjectType& rObject, CMessenger& rMessenger, 
	u32 uMessageType, void ( ObjectType::*FPush )( CMessage& ) )
{
	SListenerMessageTypePair oListener;
	oListener.m_pListener = new TListenerDelegator< ObjectType >( rObject, FPush );
	assert( oListener.m_pListener );
	oListener.m_uMessageType = uMessageType;
	oListener.m_pMessenger = &rMessenger;
	m_oListeners.Add( oListener );
	if( !m_bPaused )
	{
		rMessenger.Listen( *oListener.m_pListener, uMessageType );
	}
}

template< class ObjectType >
void CMessageReceiver::Listen( ObjectType& rObject, CMessenger& rMessenger, 
	u32 uMessageType, void ( ObjectType::*FPush )() )
{
	SListenerMessageTypePair oListener;
	oListener.m_pListener = 
		new TListenerDelegator2< ObjectType >( rObject, FPush );
	assert( oListener.m_pListener );
	oListener.m_uMessageType = uMessageType;
	oListener.m_pMessenger = &rMessenger;
	m_oListeners.Add( oListener );
	if( !m_bPaused )
	{
		rMessenger.Listen( *oListener.m_pListener, uMessageType );
	}
}

template< class ObjectType >
void CMessageReceiver::Listen( ObjectType& rObject, CMessenger& rMessenger, 
	u32 uMessageType, void ( ObjectType::*FPush )() const )
{
	SListenerMessageTypePair oListener;
	oListener.m_pListener = 
		new TListenerDelegator4< ObjectType >( rObject, FPush );
	assert( oListener.m_pListener );
	oListener.m_uMessageType = uMessageType;
	oListener.m_pMessenger = &rMessenger;
	m_oListeners.Add( oListener );
	if( !m_bPaused )
	{
		rMessenger.Listen( *oListener.m_pListener, uMessageType );
	}
}

template< class ObjectType >
void CMessageReceiver::StopListening( ObjectType& rObject, CMessenger& rMessenger, 
	u32 uMessageType, void ( ObjectType::*FPush )( CMessage&, CMessenger& ) )
{
	for( u16 i( 0 ); i < m_oListeners.GetSize(); ++i )
	{
		if( m_oListeners[ i ].m_uMessageType == uMessageType )
		{
			TListenerDelegator3< ObjectType >* pListener( 
				static_cast< TListenerDelegator3< ObjectType >* >( 
				m_oListeners[ i ].m_pListener ) );
			assert( pListener );
			if( &pListener->GetDelegate() == &rObject && 
				pListener->GetPushFunction() == FPush )
			{
				if( !m_bPaused )
				{
					assert( rMessenger.IsListening( *pListener, uMessageType ) );
					rMessenger.StopListening( *pListener, uMessageType );
				}
				delete pListener;
				m_oListeners.RemoveByIndex( i );
				return;
			}
		}
	}
}

template< class ObjectType >
void CMessageReceiver::StopListening( ObjectType& rObject, CMessenger& rMessenger, 
	u32 uMessageType, void ( ObjectType::*FPush )( CMessage& ) )
{
	for( u16 i( 0 ); i < m_oListeners.GetSize(); ++i )
	{
		if( m_oListeners[ i ].m_uMessageType == uMessageType )
		{
			TListenerDelegator< ObjectType >* pListener( 
				static_cast< TListenerDelegator< ObjectType >* >( 
				m_oListeners[ i ].m_pListener ) );
			assert( pListener );
			if( &pListener->GetDelegate() == &rObject && 
				pListener->GetPushFunction() == FPush )
			{
				if( !m_bPaused )
				{
					assert( rMessenger.IsListening( *pListener, uMessageType ) );
					rMessenger.StopListening( *pListener, uMessageType );
				}
				delete pListener;
				m_oListeners.RemoveByIndex( i );
				return;
			}
		}
	}
}

template< class ObjectType >
void CMessageReceiver::StopListening( ObjectType& rObject, CMessenger& rMessenger, 
	u32 uMessageType, void ( ObjectType::*FPush )() )
{
	for( u16 i( 0 ); i < m_oListeners.GetSize(); ++i )
	{
		if( m_oListeners[ i ].m_uMessageType == uMessageType )
		{
			TListenerDelegator2< ObjectType >* pListener( 
				static_cast< TListenerDelegator2< ObjectType >* >( 
				m_oListeners[ i ].m_pListener ) );
			assert( pListener );
			if( &pListener->GetDelegate() == &rObject && 
				pListener->GetPushFunction() == FPush )
			{
				if( !m_bPaused )
				{
					assert( rMessenger.IsListening( *pListener, uMessageType ) );
					rMessenger.StopListening( *pListener, uMessageType );
				}
				delete pListener;
				m_oListeners.RemoveByIndex( i );
				return;
			}
		}
	}
}

template< class ObjectType >
void CMessageReceiver::StopListening( ObjectType& rObject, CMessenger& rMessenger, 
	u32 uMessageType, void ( ObjectType::*FPush )() const )
{
	for( u16 i( 0 ); i < m_oListeners.GetSize(); ++i )
	{
		if( m_oListeners[ i ].m_uMessageType == uMessageType )
		{
			TListenerDelegator4< ObjectType >* pListener( 
				static_cast< TListenerDelegator4< ObjectType >* >( 
				m_oListeners[ i ].m_pListener ) );
			assert( pListener );
			if( &pListener->GetDelegate() == &rObject && 
				pListener->GetPushFunction() == FPush )
			{
				if( !m_bPaused )
				{
					assert( rMessenger.IsListening( *pListener, uMessageType ) );
					rMessenger.StopListening( *pListener, uMessageType );
				}
				delete pListener;
				m_oListeners.RemoveByIndex( i );
				return;
			}
		}
	}
}

template< class ObjectType >
bool CMessageReceiver::IsListening( const ObjectType& krObject, CMessenger& rMessenger, 
	u32 uMessageType, void ( ObjectType::*FPush )() ) const
{
	for( u16 i( 0 ); i < m_oListeners.GetSize(); ++i )
	{
		if( m_oListeners[ i ].m_uMessageType == uMessageType &&
			m_oListeners[ i ].m_pMessenger == &rMessenger )
		{
			TListenerDelegator4< ObjectType >* pListener( 
				static_cast< TListenerDelegator4< ObjectType >* >( 
				m_oListeners[ i ].m_pListener ) );
			assert( pListener );
			if( &pListener->GetDelegate() == &krObject && 
				pListener->GetPushFunction() == FPush )
			{
				return true;
			}
		}
	}
	return false;
}

template< class ObjectType >
bool CMessageReceiver::IsListening( const ObjectType& krObject, CMessenger& rMessenger, 
	u32 uMessageType, void ( ObjectType::*FPush )( CMessage&, CMessenger& ) ) const
{
	for( u16 i( 0 ); i < m_oListeners.GetSize(); ++i )
	{
		if( m_oListeners[ i ].m_uMessageType == uMessageType &&
			m_oListeners[ i ].m_pMessenger == &rMessenger )
		{
			TListenerDelegator3< ObjectType >* pListener( 
				static_cast< TListenerDelegator3< ObjectType >* >( 
				m_oListeners[ i ].m_pListener ) );
			assert( pListener );
			if( &pListener->GetDelegate() == &krObject && 
				pListener->GetPushFunction() == FPush )
			{
				return true;
			}
		}
	}
	return false;
}

template< class ObjectType >
bool CMessageReceiver::IsListening( const ObjectType& krObject, CMessenger& rMessenger, 
	u32 uMessageType, void ( ObjectType::*FPush )( CMessage& ) ) const
{
	for( u16 i( 0 ); i < m_oListeners.GetSize(); ++i )
	{
		if( m_oListeners[ i ].m_uMessageType == uMessageType && 
			m_oListeners[ i ].m_pMessenger == &rMessenger )
		{
			TListenerDelegator< ObjectType >* pListener( 
				static_cast< TListenerDelegator< ObjectType >* >( 
				m_oListeners[ i ].m_pListener ) );
			assert( pListener );
			if( &pListener->GetDelegate() == &krObject && 
				pListener->GetPushFunction() == FPush )
			{
				return true;
			}
		}
	}
	return false;
}

template< class ObjectType >
bool CMessageReceiver::IsListening( const ObjectType& krObject, CMessenger& rMessenger, 
	u32 uMessageType, void ( ObjectType::*FPush )() const ) const
{
	for( u16 i( 0 ); i < m_oListeners.GetSize(); ++i )
	{
		if( m_oListeners[ i ].m_uMessageType == uMessageType &&
			m_oListeners[ i ].m_pMessenger == &rMessenger )
		{
			TListenerDelegator2< ObjectType >* pListener( 
				static_cast< TListenerDelegator2< ObjectType >* >( 
				m_oListeners[ i ].m_pListener ) );
			assert( pListener );
			if( &pListener->GetDelegate() == &krObject && 
				pListener->GetPushFunction() == FPush )
			{
				return true;
			}
		}
	}
	return false;
}
