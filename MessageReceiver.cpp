#include "MessageReceiver.h"
#include <assert.h>
#include "Messenger/Messenger.h"

#include "Dynamic Array/DynamicArray.inl"

CMessageReceiver::SListenerMessageTypePair 
	CMessageReceiver::SListenerMessageTypePair::ms_oNull = { NULL, 0 };

CMessageReceiver::CMessageReceiver()
	: m_oListeners( 1, SListenerMessageTypePair::ms_oNull )
	, m_bPaused( false )
{
}

CMessageReceiver::~CMessageReceiver()
{
	StopListeningToAll();
}

void CMessageReceiver::StopListeningToAll()
{
	while( m_oListeners.GetSize() > 0 )
	{
		u16 uLastIndex( m_oListeners.GetSize() - 1 );
		CListener* pListener( m_oListeners[ uLastIndex ].m_pListener );
		assert( pListener );
		if( !m_bPaused )
		{
			assert( m_oListeners[ uLastIndex ].m_pMessenger );
			m_oListeners[ uLastIndex ].m_pMessenger->StopListening( *pListener, 
				m_oListeners[ uLastIndex ].m_uMessageType );
		}
		m_oListeners.RemoveByIndex( uLastIndex );
		delete pListener;
	}
}

void CMessageReceiver::Pause()
{
	assert( !m_bPaused );
	for( u16 i( 0 ); i < m_oListeners.GetSize(); ++i )
	{
		CListener* pListener( m_oListeners[ i ].m_pListener );
		assert( pListener );
		assert( m_oListeners[ i ].m_pMessenger );
		assert( m_oListeners[ i ].m_pMessenger->IsListening( *pListener, 
			m_oListeners[ i ].m_uMessageType ) );
		m_oListeners[ i ].m_pMessenger->StopListening( 
			*pListener, m_oListeners[ i ].m_uMessageType );
	}
	m_bPaused = true;
}

void CMessageReceiver::Resume()
{
	assert( m_bPaused );
	for( u16 i( 0 ); i < m_oListeners.GetSize(); ++i )
	{
		CListener* pListener( m_oListeners[ i ].m_pListener );
		assert( pListener );
		CMessenger* pMessenger( m_oListeners[ i ].m_pMessenger );
		assert( pMessenger );
		assert( !pMessenger->IsListening( *pListener, 
			m_oListeners[ i ].m_uMessageType ) );
		pMessenger->Listen( *pListener, m_oListeners[ i ].m_uMessageType );
	}
	m_bPaused = false;
}

void CMessageReceiver::StopListeningToMessenger( CMessenger& rMessenger )
{
	for( u16 i( 0 ); i < m_oListeners.GetSize(); )
	{
		if( m_oListeners[ i ].m_pMessenger == &rMessenger )
		{
			CListener* pListener( m_oListeners[ i ].m_pListener );
			assert( pListener );
			if( !m_bPaused )
			{
				assert( m_oListeners[ i ].m_pMessenger );
				m_oListeners[ i ].m_pMessenger->StopListening( *pListener, 
					m_oListeners[ i ].m_uMessageType );
			}
			m_oListeners.RemoveByIndex( i );
			delete pListener;
		}
		else
		{
			++i;
		}
	}
}
